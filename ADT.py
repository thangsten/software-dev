# CS4500 Final Project
# Courtney Sims, Thang Nguyen, Will Pardi

class ADT:

  # Static class variables:
  # list of OpSpecs that return their corresponding type
  intList = list()
  boolList = list()
  stringList = list()
  charList = list()

  # a dictionary of our defined types (i.e. StackInt, Set).
  # {keys : values} == {'typename' : [ListOf OpSpecs]}
  allDefinedTypesList = {}

  # Global list of all opSpec names
  opSpecNameList = []

  # Global list of the names of all ADTs, in order they were
  # read in from the file
  adtOrderList = []

  def __init__(self, typeName, opsList):
    self.typeName = typeName
    self.opsList = opsList

  def to_string(self):
    s = 'ADT: ' + self.typeName + '[' + '; '.join(self.opsList.to_string()) + ']'
    return s

  # adds the given name of the ADT to the adtOrderList
  def updateADTOrderList(self, name):
    if name not in ADT.adtOrderList:
      ADT.adtOrderList.append(name)


  # adds this ADT's type to the dictionary of all user-defined types.
  # its value is an empty list for now so that we can extend it later.
  def updateAllDefinedTypesList(self):
    ADT.allDefinedTypesList[self.typeName] = list()


  # for every OpSpec in this ADT's opsList, adds it to the
  # appropriate list according to its return type
  def extendTypeList(self):

    for x in self.opsList:
      if (x.returnType == 'int'):
        ADT.intList.append(x)

      elif (x.returnType == 'boolean'):
        ADT.boolList.append(x)

      elif (x.returnType == 'character'):
        ADT.charList.append(x)

      elif (x.returnType == 'string'):
        ADT.stringList.append(x)

      # if the return type is a user-defined type already in
      # our defined types dictionary, updates the value of
      # the appropriate key
      elif (x.returnType in ADT.allDefinedTypesList):
        temp = ADT.allDefinedTypesList[x.returnType]
        temp.append(x)
        ADT.allDefinedTypesList[x.returnType] = temp

      # if the return type is a user-defined type NOT already
      # in our defined types dictionary, adds it to the dictionary
      # with an empty list as its value
      else:
        ADT.allDefinedTypesList[x.returnType] = list()



  # returns an OpSpec of the given type that takes
  # no arguments
  def findArglessMethod(self, aType):
    typeOps = ADT.allDefinedTypesList[aType]
    x = 0
    foundMethod = False

    while (x < len(typeOps) and not foundMethod):
      if (not typeOps[x].args):
        foundMethod = True
        return typeOps[x]
      else:
        x = x + 1
