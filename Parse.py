# CS4500 Project
# Courtney Sims, Thang Nguyen, Will Pardi

from pyparsing import *
from random import *
from ADT import *
from OpSpec import *
from Equation import *
import random

# -----------------------------------------------------------------------------
#Globals
MAX_LEN = 50       # Number of generated expressions in the out file.
MAX_DEPTH = 5      # Max # of depth of expressions.
MAX_THRESHOLD = 50 # Max # of re-writes we will attempt.
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Defining tokens/Rules for parsing [via PyParsing]
# use setResultsName so we can refer to them later

colon = Literal(':').setResultsName('colon').suppress()
arrow = Literal('->').setResultsName('arrow').suppress()
lParen = Literal('(').setResultsName('lParen').suppress()
rParen = Literal(')').setResultsName('rParen').suppress()
empty = Empty().setResultsName('empty')
identifier = Word(alphas, alphanums).setResultsName('ident')
trueBool = Literal('#t').setResultsName('trueBool')
falseBool = Literal('#f').setResultsName('falseBool')
notStr = Literal('not').setResultsName('notString')
plus = Literal('+').setResultsName('plus')
minus = Literal('-').setResultsName('minus')
asterisk = Literal('*').setResultsName('asterisk')
equalSign = Literal('=').setResultsName('equalSign')
lessThan = Literal('<').setResultsName('lessThan')
greaterThan = Literal('>').setResultsName('greaterThan')
uInteger10 = Word(nums).setResultsName('uInt')

adtString = Literal('ADT').setResultsName('adtString')
intStr = Literal('int').setResultsName('intStr')
boolStr = Literal('boolean').setResultsName('boolStr')
charStr = Literal('character').setResultsName('charStr')
strStr = Literal('string').setResultsName('strStr')

typeName = (identifier).setResultsName('typeName')
op = (identifier).setResultsName('op')
type_ = (Literal(['int','boolean','character','string']) | typeName).setResultsName('type_')
argTypes = Group( type_ + ZeroOrMore(asterisk.suppress() + type_)).setResultsName('argTypes')

signatureString = Literal('Signatures').setResultsName('signatureString')
opSpec = Group(op + colon + Optional(argTypes).setResultsName('args') + arrow + type_).setResultsName('opSpec')
opSpecs = OneOrMore(opSpec).setResultsName('opSpecs', listAllMatches=True)
signature = Group((Combine(adtString + colon).suppress() + Group(typeName) + Group(opSpecs))).setResultsName('signature')
signatures = OneOrMore(signature).setResultsName('signatures', listAllMatches=True)

equationString = Literal('Equations').setResultsName('equationString')
term = Forward()
args = ZeroOrMore(term).setResultsName('args', listAllMatches=True)
term << (identifier | Group(lParen + op + args + rParen).setResultsName('moreTerm'))
primitive = (notStr | plus | minus | asterisk | equalSign | lessThan | greaterThan).setResultsName('primitive')

rhs = Forward()
rhsArgs = ZeroOrMore(rhs).setResultsName('rhsArgs', listAllMatches=True)
rhs << Group(trueBool | falseBool | uInteger10 | identifier | (lParen + op + rhsArgs + rParen) | (lParen + primitive + rhsArgs + rParen)).setResultsName('rhs')

equation = Group(term + equalSign.suppress() + rhs).setResultsName('equation')
equations = ZeroOrMore(equation).setResultsName('equations', listAllMatches=True)

input_ = (Combine(signatureString + colon).suppress() + Group(signatures) + Combine(equationString + colon).suppress() + Group(equations)).setResultsName('input_', listAllMatches=True)
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------

# given input read in from a file and tokenized,
# generates Scheme expressions for the given ADT
def inputParse(inputStr):

  # parses signatures and equations
  for x in inputStr:
    if x.getName() == 'signatures':
      signaturesParse(x)
    elif x.getName() == 'equations':
      equationsParse(x)

  # generates Scheme expressions
  return generate(MAX_LEN, MAX_DEPTH)

# Generates up to 100 expressions for types that we can
# generate expressions for.
# The numExprs argument specifies the maximum number of expressions
# to print.
# The depth argument is used for determining the size of each
# generated expression.  The bigger num is, the longer the
# generated expressions will be.
# (Depth between 5 and 15 is good. Anything bigger starts getting too long)
def generate(maxNumExprs, depth):
  count = 0
  OUT = []
  type_list = ADT.allDefinedTypesList
  imports =  ADT.adtOrderList
  while count < maxNumExprs:
    # picks a random list to generate from (int, boolean, string,
    # character, or the dictionary of defined types
    randType = randint(0, len(type_list)-1)

    # if the randType is the dictionary of user defined types
    if (randType == (len(type_list)-1)):
      # generate a random user defined type
      randType2 = randint(0, len(type_list)-1)
      # generate an expr of the random user defined type
      if (len(type_list.values()[randType2]) > 0):
        expr = generateType(type_list.keys()[randType2], depth)
        if (isinstance(expr,Expr) and (not expr.isin(OUT))):
          OUT.append(expr)
          OUT.append(rewrite(expr))
          count = count + 1

    # if the randType is int, boolean, string, or character, generate
    # an expr of that type
    else:
      expr = generateType(type_list.keys()[randType], depth)
      if (isinstance(expr,Expr) and (not expr.isin(OUT))):
        OUT.append(expr)
        OUT.append(rewrite(expr))
        count = count + 1

  return (imports,OUT)


# This will match an Expr to an Equation in Equation.eqList (global)
# Returns an Equation if there's a match, else returns None
def matchEq(exp):
  for eq in Equation.eqList:
    if exp.match(eq.fromExpr):
      return eq

# This will check if the arguments of an Expr have a match at a lower level
def matchBelow(args):
  for arg in args:
    if isinstance(arg,Expr):
      if matchEq(arg) is not None:
        return True
  return False

# This will take in an Expression, and will reduce the
# rewriteable parts and return
def reduce(exp):
  if not matchBelow(exp.args) and matchEq(exp):
    return matchEq(exp).rewrite(exp)

  elif not matchBelow(exp.args):
    return exp

  # Else there IS a matchBelow
  else:
    out = []

    for arg in exp.args:
      if isinstance(arg,Expr):
        out += [reduce(arg)]

      else:
        out += [arg]

    return Expr(exp.op, out)

# Recursively reduce the most inner possible case
# Continue until the threshold has been reached, or
# the rewrite equals the given.
def rewrite(exp):
  counter = MAX_THRESHOLD
  out = reduce(exp)
  while(counter):
    if exp.isEqual(out):
      return exp

    else:
      out = reduce(out)
    counter -= 1

  return out
# -----------------------------------------------------------------------------

# parses a list of Equations
def equationsParse(eqs):
  for x in eqs:
    if (x.getName() == 'equation'):
      equationParse(x)



# parses an Equation
def equationParse(eq):
  templhs = None
  temprhs = None

  for x in eq:
    if x.getName() == 'moreTerm':
      templhs = lhsParse(x.asList())

    if x.getName() == 'rhs':
      temprhs = rhsParse(x.asList())
      Equation.eqList += [Equation(templhs,temprhs)]

# Parses the RHS of an equation.
def rhsParse(rhs):
  out = []
  if len(rhs) == 1:
    if rhs[0] in ADT.opSpecNameList:
      return Expr(rhs[0], [])
    else:
      return rhs[0]

  else:
    for x in rhs[1:]:
      if isinstance(x,list):
        out += [rhsParse(x)]
      else:
        out += [x]

  return Expr(rhs[0], out)

# Parses the LHS of an equation.
def lhsParse(lhs):
  out = []
  for x in lhs[1:]:
    if isinstance(x,list):
      out += [lhsParse(x)]
    else:
      out += [x]

  return Expr(lhs[0], out)
# -----------------------------------------------------------------------------

# parses each ADT
def signaturesParse(sigs):
  for x in sigs:
    ADTParse(x)

# parses an ADT into its typeName and OpSpecs,
# adds its typeName to the dictionary of user-defined types, and
# extends the appropriate static type lists with its OpSpecs.
def ADTParse(sig):

  for x in sig:
    if (x.getName() == 'typeName'):
      definedTypeName = x[0]

    elif (x.getName() == 'opSpecs'):
      opsList = makeOpSpecs(x)

  anADT = ADT(definedTypeName, opsList)
  anADT.updateAllDefinedTypesList()
  anADT.updateADTOrderList(definedTypeName)
  anADT.extendTypeList()

# for every opSpec token in the list, creates a new OpSpec object
# and returns them all in a new list
def makeOpSpecs(ops):
  opList = list()

  for x in ops:
    if (x.getName() == 'opSpec'):
      opList.append(makeOpSpec(x))

  return opList

# makes an OpSpec object out of an opSpec token and returns it
def makeOpSpec(op):
  anOpSpec = OpSpec('', list(), '')

  if (len(op) == 2):
    anOpSpec = OpSpec(op[0], list(), op[1])

  elif (len(op) == 3):
    anOpSpec = OpSpec(op[0], op[1], op[2])

  ADT.opSpecNameList += [anOpSpec.opName]
  return anOpSpec

# generate an expression of the given type.
# argument n keeps us from generating an expression of infinite length.
# (everyone loves infinite loops!)
def generateType(aType, n):
  length = 0
  rand = randint(0,1)
  randomOp = ''

  if (aType == 'int'):
    aList = ADT.intList
    length = len(aList)

    if (rand == 0 or length == 0 or n <= 0):
      return str(randint(0,9001))

    else:
      x = randint(0, length-1)
      randomOp = aList[x]

  elif (aType == 'boolean'):
    aList = ADT.boolList
    length = len(aList)

    if (rand == 0 or length == 0 or n <= 0):
      return choice(['#t', '#f'])

    else:
      x = randint(0, length-1)
      randomOp = aList[x]

  elif (aType == 'character'):
    aList = ADT.charList
    length = len(aList)

    if (rand == 0 or length == 0 or n <= 0):
      return '#\j'

    else:
      x = randint(0, length-1)
      randomOp = aList[x]

  elif (aType == 'string'):
    aList = ADT.stringList
    length = len(aList)

    if (rand == 0 or length == 0 or n <= 0):
      return 'hi'

    else:
      x = randint(0, length-1)
      randomOp = aList[x]

  elif (aType in ADT.allDefinedTypesList):
    aList = ADT.allDefinedTypesList[aType]
    length = len(aList)
    # if n has reached 0 we want to generate an expr that doesn't take
    # any arguments so we can force-stop recursion & avoid infinite loops.

    if (n <= 0):
      randomADT = ADT('', list())
      randomOp = randomADT.findArglessMethod(aType)

    # otherwise generate any random expression of the given type
    else:
      if not aList:
        pass

      else:
        randomOp = random.choice(aList)

  else:
    raise Exception('generateType : cannot generate expression of ' + aType)

  # check if the Op takes arguments.
  # (not randomOp.args) returns true if empty, false if not empty
  if isinstance(randomOp,str):
    pass

  elif (not randomOp.args):
    return Expr(randomOp.opName, [])

  else:
    return Expr(randomOp.opName, generateArgTypes(randomOp.args, n-1))

# generates expressions according to the argument types of an operation.
# argument n keeps us from generating an expression of infinite length.
def generateArgTypes(argList, n):
  args = []
  for x in argList:
    args.append(generateType(x, n-1))
  return args
