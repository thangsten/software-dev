# CS4500 Final Project
# Courtney Sims, Thang Nguyen, Will Pardi


# represents an opSpec
class OpSpec:

  # initialise OpSpec
  def __init__(self, opName, args, returnType):
    self.opName = opName
    self.args = args
    self.returnType = returnType


  # represents this OpSpec as a string
  def to_string(self):
    s = '(' + self.opName + ' ' + ' '.join(self.args) + ') => ' + self.returnType
    return s
