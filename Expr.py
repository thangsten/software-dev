# CS4500 Final Project
# Courtney Sims, Thang Nguyen, Will Pardi

class Expr(object):
  '''
  Expr has:
  - op   : String
  - args : ListOf (Exprs | primitive)
  (add 2 (sub 3 4))
  a = (Expr 'add', ['2', (Expr 'sub' , ['3','4'])])
  a.find('2') = ['0']
  a.find('3') = ['1','0']
  '''
  def __init__(self, op, args):
    self.op = op
    self.args = args

  # Deep equality check against the given expr to itself.
  def isEqual(self, expr2):
    if isinstance(expr2,str):
      return False

    else:
      return self.op == expr2.op and self.argEquals(self.args, expr2.args)

  # Helper for isEqual, checks equality among arguments.
  def argEquals(self, args1, args2):
    result = True

    if len(args1) != len(args2):
      return False

    else:
      for index in range(0,len(args1)):
        if isinstance(args1[index], Expr) and isinstance(args2[index], Expr):
          result = result and args1[index].isEqual(args2[index])

        elif isinstance(args1[index], str) and isinstance(args2[index], str):
          result = result and (args1[index] == args2[index])

        else:
          return False

      return result

  # Is this Expr in the given list of Exprs?
  def isin(self, alist):
    result = False
    for x in alist:
      if self.isEqual(x):
        result = True
    return result

  # Finds a non-Expr inside an Expr. path is initially an empty list
  # eg te = Expr('+', ['1', Expr('asInt', ['m'])])
  # te.find('1','[]') = [0]
  # te.find('m','[]') = [1,1]
  def find(self, val, path):
    result = None
    for index in range(0,len(self.args)):
      if isinstance(self.args[index], Expr):
          result = self.args[index].find(val, path + [index])

      elif self.args[index] == val:
        return path + [index]

    if result:
      return result

  # Matches an expression to a fromExpr in Equation.
  def match(self, matchExp):
    if isinstance(matchExp, str):
      return True

    else:
      return matchExp.op == self.op and self.matchHelper(self.args, matchExp.args)


  def matchHelper(self, sArg, mArg):
    result = True

    if len(sArg) != len(mArg):
      return False

    for index in range(0, len(mArg)):
      if isinstance(mArg[index], Expr) and isinstance(sArg[index],Expr):
        result = result and sArg[index].match(mArg[index])

      elif isinstance(mArg[index], str):
        result = result and True

      else:
        result = False

    return result

  # Based on output from find [a path], gets the Expr or Element from this Expr
  def get(self, dirs):
    out = self.args

    for x in dirs:
      if isinstance(out, Expr):
        out = out.args[x]

      else:
        out = out[x]

    return out

  def toString(self):
      return '(%s%s)' % (self.op, self.argsToString())

  def argsToString(self):
    if not self.args:
      return ''

    out = ''
    for arg in self.args:
      out += ' '

      if isinstance(arg, str):
        out += arg

      else:
        out += arg.toString()

    return out
