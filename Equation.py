# CS4500 Final Project
# Courtney Sims, Thang Nguyen, Will Pardi

from Expr import *

# represents an Equation
class Equation:
  # Global static variable containing all equations
  eqList = []

  # initialise Equation
  def __init__(self, fromExpr, toExpr):
    self.fromExpr = fromExpr
    self.toExpr = toExpr

  # Re-writes a given expression to match the toExpr [with its args]
  # This is only called when match in Expr is True
  def rewrite(self, exp):
    # Base case for "simple" RHS equations.
    if isinstance(self.toExpr, str):
      if self.fromExpr.find(self.toExpr,[]) is None:
        return exp

      else:
        return exp.get(self.fromExpr.find(self.toExpr,[]))

    # Need an alternate case when the RHS has no arguments from the LHS
    # Eg (foo (bar x)) = 1
    else:
      return Expr(self.toExpr.op, self.rewriteArgs(self.toExpr.args, exp))


  # Recursive helper that iterates over the list of args
  # and Rewrites based on the placement of the RHS arguments.
  def rewriteArgs(self, rwargs, exp):
    out = []
    for arg in rwargs:
      if isinstance(arg, Expr):
        out += [Expr(arg.op, self.rewriteArgs(arg.args, exp))]

      elif self.fromExpr.find(arg,[]) is None:
        out += arg

      else:
        out += [exp.get(self.fromExpr.find(arg,[]))]

    return out

  def toString(self):
    if isinstance(self.toExpr,str):
      return '%s = %s' % (self.fromExpr.toString(), self.toExpr)
    else:
      return '%s = %s' % (self.fromExpr.toString(), self.toExpr.toString())
